# frozen_string_literal: true

require_relative "gem/version"

module Example
  module Gem
    class Error < StandardError; end
    # Your code goes here...
  end
end
