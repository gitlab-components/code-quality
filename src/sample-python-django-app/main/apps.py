from django.apps import AppConfig


class MainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'

    # only used here to trigger a code smell
    def code_smell(a):
      i = 10
      return i + a       # Noncompliant
      i += 1             # this is never executed
